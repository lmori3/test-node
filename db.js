const mysql = require('mysql2');

module.exports = {

    dbConnection: mysql.createConnection({
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
        // host: 'localhost',
        // port: '52738',
        // user: 'mysql_user',
        // password: 'mysql_pwd',
        // database: 'sampledb'
    }),

    getAllData: function (connection) {
        return new Promise((resolve, reject) => {
            console.log("Retrieving all datas");
            connection.query("select * from TEST_TABLE;", function (err, queryRes, fields) {
                if (err) {
                    console.log("Error in query: " + err.message);
                    reject(err);
                } else if (queryRes) {
                    var result = [];
                    queryRes.forEach(res => {
                        if (res) {
                            result.push({
                                id: res.ID,
                                description: res.DESCRIPTION,
                                status: res.STATUS
                            });
                        }
                    });
                    console.log("Result: " + JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    },

    getDataByStatus: function (connection, status) {
        return new Promise((resolve, reject) => {
            console.log("Retrieving data by status: " + status);
            connection.query("select * from TEST_TABLE where STATUS=?;", [status], function (err, queryRes, fields) {
                if (err) {
                    console.log("Error in query: " + err.message);
                    reject(err);
                } else if (queryRes) {
                    var result = [];
                    queryRes.forEach(res => {
                        if (res) {
                            result.push({
                                id: res.ID,
                                description: res.DESCRIPTION,
                                status: res.STATUS
                            });
                        }
                    });
                    console.log("Result: " + JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    },

    getDataByID: function (connection, id) {
        return new Promise((resolve, reject) => {
            console.log("Retrieving data by id: " + id);
            connection.query("select * from TEST_TABLE where ID=?;", [id], function (err, queryRes, fields) {
                if (err) {
                    console.log("Error in query: " + err.message);
                    reject(err);
                } else if (queryRes) {
                    var result = [];
                    queryRes.forEach(res => {
                        if (res) {
                            result.push({
                                id: res.ID,
                                description: res.DESCRIPTION,
                                status: res.STATUS
                            });
                        }
                    });
                    console.log("Result: " + JSON.stringify(result));
                    resolve(result[0]);
                }
            });
        });
    }
}