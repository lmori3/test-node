const { ApolloServer, gql } = require('apollo-server-express');
const {
    ApolloServerPluginDrainHttpServer,
    ApolloServerPluginLandingPageLocalDefault
} = require('apollo-server-core');
const express = require('express');
const http = require('http');
const db = require('./db');

async function startApolloServer(typeDefs, resolvers) {
    // Required logic for integrating with Express
    const app = express();
    // Our httpServer handles incoming requests to our Express app.
    // Below, we tell Apollo Server to "drain" this httpServer,
    // enabling our servers to shut down gracefully.
    const httpServer = http.createServer(app);

    // Same ApolloServer initialization as before, plus the drain plugin
    // for our httpServer.
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        csrfPrevention: true,
        cache: 'bounded',
        plugins: [
            ApolloServerPluginDrainHttpServer({ httpServer }),
            ApolloServerPluginLandingPageLocalDefault({ embed: true }),
        ],
    });

    // More required logic for integrating with Express
    await server.start();
    server.applyMiddleware({
        app,

        // By default, apollo-server hosts its GraphQL endpoint at the
        // server root. However, *other* Apollo Server packages host it at
        // /graphql. Optionally provide this to match apollo-server.
        path: '/',
    });

    // Modified server startup
    await new Promise(resolve => httpServer.listen({ port: 4000 }, resolve));
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
}

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  enum Status {
    PENDING
    WORKING
    DONE
  }

  # This "Data" type defines the queryable fields for every Data in our data source.
  type Data {
    id: String
    description: String
    status: Status
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "Datas" query returns an array of zero or more Data (defined above).
  type Query {
    datas: [Data],
    dataByStatus(status:Status!): [Data],
    dataByID(id:String!): Data
  }
`;

// const datas = [
//     {
//         id: '1',
//         description: 'AAA',
//         status: 'PENDING',
//     },
//     {
//         id: '2',
//         description: 'BBB',
//         status: 'PENDING',
//     },
//     {
//         id: '3',
//         description: 'CCC',
//         status: 'WORKING',
//     },
// ];

// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves datas from the "Datas" array above.
const resolvers = {
    Query: {
        datas: () => {
            return db.getAllData(db.dbConnection);
        },
        dataByStatus: (root, args, context, info) => {
            return db.getDataByStatus(db.dbConnection, args.status);
            // return datas.filter(data => data.status === args.status);
        },
        dataByID: (root, args, context, info) => {
            return db.getDataByID(db.dbConnection, args.id);
            // return datas.find(data => data.id === args.id);
        },
    },
};

// START
db.dbConnection.connect(function (err) {
    if (err) {
        return console.log("Error connecting DB: " + err.message);
    } else {
        console.log("Connected to DB! Starting...");
        startApolloServer(typeDefs, resolvers);
    }
});
